TEMPLATE = app
TARGET = Doomwhere
QT += quick gui widgets quickcontrols2

LIBS += -lz -ldl

RESOURCES += \
   qtquickcontrols2.conf \
   $$files(*.qml)

#SUBDIRS += \
#    lib/plugins/zandronum/zandronum.pro

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH = $$PWD/designer

OTHER_FILES += \
    designer/Backend/*.qml

target.path = $$[QT_INSTALL_EXAMPLES]/quickcontrols2/Doomwhere
INSTALLS += target
DEFINES += INSTALL_PREFIX=$$(PREFIX)
DEFINES += INSTALL_LIBDIR=lib
DEFINES += PLUGIN_VERSION=39
CONFIG += c++17

HEADERS += \
    configuration/doomseekerconfig.h \
    configuration/passwordscfg.h \
    configuration/queryspeed.h \
    configuration/serverpassword.h \
    configuration/serverpasswordsummary.h \
    gui/entity/serverlistfilterinfo.h \
    gui/entity/showmode.h \
    gui/helpers/comboboxex.h \
    gui/helpers/datetablewidgetitem.h \
    gui/helpers/playersdiagram.h \
    gui/helpers/taskbarbutton.h \
    gui/helpers/taskbarprogress.h \
    gui/models/serverlistcolumn.h \
    gui/models/serverlistmodel.h \
    gui/models/serverlistproxymodel.h \
    gui/models/serverlistrowhandler.h \
    ini/ini.h \
    ini/inisection.h \
    ini/inivariable.h \
    ini/settingsprovider.h \
    ini/settingsproviderqt.h \
    ip2c/entities/ip2ccountryinfo.h \
    ip2c/ip2c.h \
    ip2c/ip2cloader.h \
    ip2c/ip2cparser.h \
    ip2c/ip2cupdater.h \
    irc/configuration/chatlogscfg.h \
    irc/configuration/chatnetworkscfg.h \
    irc/configuration/ircconfig.h \
    irc/constants/ircresponsetype.h \
    irc/entities/ircnetworkentity.h \
    irc/entities/ircresponseparseresult.h \
    irc/entities/ircuserprefix.h \
    irc/ops/ircdelayedoperation.h \
    irc/ops/ircdelayedoperationban.h \
    irc/ops/ircdelayedoperationignore.h \
    irc/chatlogarchive.h \
    irc/chatlogrotate.h \
    irc/chatlogs.h \
    irc/chatnetworknamer.h \
    irc/ircadapterbase.h \
    irc/ircchanneladapter.h \
    irc/ircchatadapter.h \
    irc/ircclient.h \
    irc/ircctcpparser.h \
    irc/ircglobal.h \
    irc/ircisupportparser.h \
    irc/ircmessageclass.h \
    irc/ircnetworkadapter.h \
    irc/ircnetworkconnectioninfo.h \
    irc/ircnicknamecompleter.h \
    irc/ircprivadapter.h \
    irc/ircrequestparser.h \
    irc/ircresponseparser.h \
    irc/ircuserinfo.h \
    irc/ircuserlist.h \
    pathfinder/basefileseeker.h \
    pathfinder/caseinsensitivefsfileseeker.h \
    pathfinder/casesensitivefsfileseeker.h \
    pathfinder/filealias.h \
    pathfinder/filesearchpath.h \
    pathfinder/pathfind.h \
    pathfinder/pathfinder.h \
    pathfinder/wadpathfinder.h \
    plugins/zandronum/huffman/bitreader.h \
    plugins/zandronum/huffman/bitwriter.h \
    plugins/zandronum/huffman/codec.h \
    plugins/zandronum/huffman/huffcodec.h \
    plugins/zandronum/huffman/huffman.h \
    plugins/zandronum/zandronum2dmflags.h \
    plugins/zandronum/zandronum3dmflags.h \
    plugins/zandronum/zandronumengineplugin.h \
    plugins/zandronum/zandronumgameinfo.h \
    plugins/zandronum/zandronummasterclient.h \
    plugins/zandronum/zandronumserver.h \
    plugins/zandronum/zandronumserverdmflagsparser.h \
    plugins/enginedefaults.h \
    plugins/engineplugin.h \
    plugins/pluginloader.h \
    refresher/canrefreshserver.h \
    refresher/hostport.h \
    refresher/refresher.h \
    refresher/udpsocketpool.h \
    serverapi/tooltips/dmflagshtmlgenerator.h \
    serverapi/tooltips/gameinfotip.h \
    serverapi/tooltips/generalinfotip.h \
    serverapi/tooltips/playertable.h \
    serverapi/tooltips/servertooltip.h \
    serverapi/tooltips/tooltipgenerator.h \
    serverapi/broadcast.h \
    serverapi/broadcastmanager.h \
    serverapi/joinerror.h \
    serverapi/masterclient.h \
    serverapi/mastermanager.h \
    serverapi/message.h \
    serverapi/player.h \
    serverapi/playerslist.h \
    serverapi/polymorphism.h \
    serverapi/rconprotocol.h \
    serverapi/server.h \
    serverapi/serverlistcounttracker.h \
    serverapi/serverptr.h \
    serverapi/serverstructs.h \
    serverapi/serversummary.h \
    serverapi/textprovider.h \
    contactmodel.h \
    customservers.h \
    datapaths.h \
    datastreamoperatorwrapper.h \
    doomseekerfilepaths.h \
    dptr.h \
    fileutils.h \
    gitinfo.h \
    global.h \
    localizationinfo.h \
    log.h \
    patternlist.h \
    random.h \
    scanner.h \
    socketsignalsadapter.h \
    strings.hpp \
    utf8splitter.h \
    version.h \
    versiondefs.h \
    main.h \
    gui/serverlist.h

SOURCES += \
    configuration/doomseekerconfig.cpp \
    configuration/passwordscfg.cpp \
    configuration/queryspeed.cpp \
    configuration/serverpassword.cpp \
    configuration/serverpasswordsummary.cpp \
    gui/entity/serverlistfilterinfo.cpp \
    gui/entity/showmode.cpp \
    gui/helpers/comboboxex.cpp \
    gui/helpers/datetablewidgetitem.cpp \
    gui/helpers/playersdiagram.cpp \
    gui/helpers/taskbarbutton.cpp \
    gui/helpers/taskbarprogress.cpp \
    gui/models/serverlistcolumn.cpp \
    gui/models/serverlistmodel.cpp \
    gui/models/serverlistproxymodel.cpp \
    gui/models/serverlistrowhandler.cpp \
    ini/ini.cpp \
    ini/inisection.cpp \
    ini/inivariable.cpp \
    ini/settingsprovider.cpp \
    ini/settingsproviderqt.cpp \
    ip2c/entities/ip2ccountryinfo.cpp \
    ip2c/ip2c.cpp \
    ip2c/ip2cloader.cpp \
    ip2c/ip2cparser.cpp \
    ip2c/ip2cupdater.cpp \
    irc/configuration/chatlogscfg.cpp \
    irc/configuration/chatnetworkscfg.cpp \
    irc/configuration/ircconfig.cpp \
    irc/constants/ircresponsetype.cpp \
    irc/entities/ircnetworkentity.cpp \
    irc/entities/ircresponseparseresult.cpp \
    irc/entities/ircuserprefix.cpp \
    irc/ops/ircdelayedoperation.cpp \
    irc/ops/ircdelayedoperationban.cpp \
    irc/ops/ircdelayedoperationignore.cpp \
    irc/chatlogarchive.cpp \
    irc/chatlogrotate.cpp \
    irc/chatlogs.cpp \
    irc/chatnetworknamer.cpp \
    irc/ircadapterbase.cpp \
    irc/ircchanneladapter.cpp \
    irc/ircchatadapter.cpp \
    irc/ircclient.cpp \
    irc/ircctcpparser.cpp \
    irc/ircglobal.cpp \
    irc/ircisupportparser.cpp \
    irc/ircmessageclass.cpp \
    irc/ircnetworkadapter.cpp \
    irc/ircnicknamecompleter.cpp \
    irc/ircprivadapter.cpp \
    irc/ircrequestparser.cpp \
    irc/ircresponseparser.cpp \
    irc/ircuserinfo.cpp \
    irc/ircuserlist.cpp \
    pathfinder/basefileseeker.cpp \
    pathfinder/caseinsensitivefsfileseeker.cpp \
    pathfinder/casesensitivefsfileseeker.cpp \
    pathfinder/filealias.cpp \
    pathfinder/filesearchpath.cpp \
    pathfinder/pathfind.cpp \
    pathfinder/pathfinder.cpp \
    pathfinder/wadpathfinder.cpp \
    plugins/zandronum/huffman/bitreader.cpp \
    plugins/zandronum/huffman/bitwriter.cpp \
    plugins/zandronum/huffman/huffcodec.cpp \
    plugins/zandronum/huffman/huffman.cpp \
    plugins/zandronum/zandronum2dmflags.cpp \
    plugins/zandronum/zandronum3dmflags.cpp \
    plugins/zandronum/zandronumengineplugin.cpp \
    plugins/zandronum/zandronumgameinfo.cpp \
    plugins/zandronum/zandronummasterclient.cpp \
    plugins/zandronum/zandronumserver.cpp \
    plugins/zandronum/zandronumserverdmflagsparser.cpp \
    plugins/engineplugin.cpp \
    plugins/pluginloader.cpp \
    refresher/canrefreshserver.cpp \
    refresher/hostport.cpp \
    refresher/refresher.cpp \
    refresher/udpsocketpool.cpp \
    serverapi/tooltips/dmflagshtmlgenerator.cpp \
    serverapi/tooltips/gameinfotip.cpp \
    serverapi/tooltips/generalinfotip.cpp \
    serverapi/tooltips/playertable.cpp \
    serverapi/tooltips/servertooltip.cpp \
    serverapi/tooltips/tooltipgenerator.cpp \
    serverapi/broadcast.cpp \
    serverapi/broadcastmanager.cpp \
    serverapi/joinerror.cpp \
    serverapi/masterclient.cpp \
    serverapi/mastermanager.cpp \
    serverapi/message.cpp \
    serverapi/player.cpp \
    serverapi/playerslist.cpp \
    serverapi/rconprotocol.cpp \
    serverapi/server.cpp \
    serverapi/serverlistcounttracker.cpp \
    serverapi/serverstructs.cpp \
    serverapi/serversummary.cpp \
    contactmodel.cpp \
    customservers.cpp \
    datapaths.cpp \
    datastreamoperatorwrapper.cpp \
    doomseekerfilepaths.cpp \
    fileutils.cpp \
    localizationinfo.cpp \
    log.cpp \
    main.cpp \
    patternlist.cpp \
    random.cpp \
    scanner.cpp \
    strings.cpp \
    utf8splitter.cpp \
    version.cpp \
    plugins/zandronum/zandronumgamesettings.cpp \
    gui/serverlist.cpp

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

contains(ANDROID_TARGET_ARCH,arm64-v8a) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
}
