/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "contactmodel.h"
#include "serverapi/playerslist.h"

ServersModel::ServersModel(QObject *parent ) : QAbstractListModel(parent)
{
}

int ServersModel::rowCount(const QModelIndex &) const
{
    return list.count();
}

QVariant ServersModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < rowCount())
        switch (role) {
        case NameRole: return list.at(index.row())->name();
        case HostnameRole: return list.at(index.row())->hostName();
        case AddressWithPortRole: return list.at(index.row())->addressWithPort();
        case BotCountRole: return list.at(index.row())->players().numBots();
        case PlayerCountRole: return list.at(index.row())->players().numClientsWithoutBots();
        case PingRole: return list.at(index.row())->ping();
        case WadsRole: return list.at(index.row())->allWadNames().join("\n");
        case CountryRole: return list.at(index.row())->isLan()? "LAN" : "Internet Zandronum Servers";
        default: return QVariant();
    }
    return QVariant();
}

QHash<int, QByteArray> ServersModel::roleNames() const
{
    static const QHash<int, QByteArray> roles {
        { NameRole, "name" },
        { HostnameRole, "hostname" },
        { AddressWithPortRole, "addressWithPort" },
        { BotCountRole, "botCount" },
        { PlayerCountRole, "playerCount" },
        { PingRole, "ping" },
        { WadsRole, "wads" },
        { CountryRole, "country" }
    };
    return roles;
}

QVariantMap ServersModel::get(int row) const
{
    ServerCPtr item = list.value(row);
    return { {"name", item->name()}, { "hostname", item->hostName() }, {"address", item->addressWithPort()}, {"botCount", item->players().numBots()}, {"playerCount", item->players().numClientsWithoutBots()}, {"wads", item->allWadNames().join("\n")}, {"ping", item->ping()} };
}

void ServersModel::append(const ServerPtr &item)
{
    int row = 0;
    unsigned ping = item->ping();
    while (row < list.count() && ping > list.at(row)->ping())
        ++row;
    beginInsertRows(QModelIndex(), row, row);
    list.insert(row, item);
    endInsertRows();
}

void ServersModel::set(int row, const ServerPtr &item)
{
    if (row < 0 || row >= list.count())
        return;

    list.replace(row, item);
    dataChanged(index(row, 0), index(row, 0), { NameRole, HostnameRole, AddressWithPortRole, BotCountRole, PlayerCountRole, PingRole, WadsRole, CountryRole });
}

void ServersModel::remove(int row)
{
    if (row < 0 || row >= list.count())
        return;

    beginRemoveRows(QModelIndex(), row, row);
    list.removeAt(row);
    endRemoveRows();
}

ServerPtr ServersModel::serverFromList(int rowIndex) const
{
    return list.at(rowIndex);
}

ServerPtr ServersModel::serverFromList(const QModelIndex& index) const
{
    return serverFromList(index.row());
}

