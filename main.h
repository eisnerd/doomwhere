#ifndef MAIN_H
#define MAIN_H

#include "contactmodel.h"

#include "log.h"
#include "refresher/refresher.h"
#include "configuration/doomseekerconfig.h"
#include "configuration/queryspeed.h"
#include "datapaths.h"
#include "serverapi/mastermanager.h"
#include "plugins/pluginloader.h"
#include "plugins/engineplugin.h"
#include "plugins/zandronum/zandronumengineplugin.h"
#include "ini/inisection.h"
#include "serverapi/server.h"
#include "gui/serverlist.h"
#include "gui/models/serverlistproxymodel.h"
#include "gui/models/serverlistcolumn.h"

#include <cstdio>

class Main : public QObject {
    Q_OBJECT

MasterManager *masterManager = new MasterManager();
//ServerList *serverList = new ServerList();
ServersModel *serversModel = new ServersModel();

void setupRefreshingThread()
{
        gLog << tr("Starting refreshing thread.");
        gRefresher->setDelayBetweenResends(gConfig.doomseeker.querySpeed().delayBetweenSingleServerAttempts);
        gRefresher->start();
}

void initGames()
{
    PluginLoader::init(gDefaultDataPaths->pluginSearchLocationPaths());

    gLog << tr("Initializing configuration for plugins.");
    gPlugins->initConfig();

    EnginePlugin *z = doomSeekerInit();
    IniSection cfgSection = gConfig.iniSectionForPlugin("Zandronum");
    z->setConfig(cfgSection);


    for(unsigned i = 0; i <= gPlugins->numPlugins(); ++i)
    {
                    const EnginePlugin* plugin = i < gPlugins->numPlugins() ? gPlugins->info(i) : z;
                    if(!plugin->data()->hasMasterClient() && !plugin->data()->hasBroadcast())
                    {
                            continue;
                    }

                    if (plugin->data()->hasMasterClient())
                    {
                            MasterClient* pMasterClient = plugin->data()->masterClient;
                            pMasterClient->updateAddress();
                            masterManager->addMaster(pMasterClient);
                    }
    }
}

void refresh()
{
    if (masterManager->numMasters() == 0) {
        gLog << tr("Err... no master servers.");
    }
    for (int i = 0; i < masterManager->numMasters(); ++i) {
        MasterClient* pMaster = (*masterManager)[i];
        if (pMaster->isEnabled()) {
            gRefresher->registerMaster(pMaster);
        }
    }
}

ServerListProxyModel *proxy = new ServerListProxyModel(serversModel);
void init()
{
    proxy->setSourceModel(serversModel);
    QRegExp pattern(QString("*") + "qc" + "*", Qt::CaseInsensitive, QRegExp::Wildcard);
    proxy->setFilterRegExp(pattern);
    //proxy->setSortRole(ServersModel::PingRole);
    //proxy->sortServers(0);
    connect(gRefresher, SIGNAL( finishedQueryingMaster(MasterClient*) ), this, SLOT( finishedQueryingMaster(MasterClient*) ) );
}

public slots:
void finishedQueryingMaster(MasterClient* master)
{
        if (master == nullptr)
        {
                return;
        }

        for(int i = 0;i < master->numServers();i++)
        {
            ServerPtr server = (*master)[i];
            //serverList->registerServer(server);
//            gLog << server->addressWithPort(); //d->serverList->registerServer((*master)[i]);
            this->connect(server.data(), SIGNAL(updated(ServerPtr, int)), SLOT(onServerUpdated(ServerPtr)));
        }
}

void onServerUpdated(const ServerPtr &server)
{
    //gLog << "Updated " << server->addressWithPort(); //d->serverList->registerServer((*master)[i]);
    //cm.append(server->addressWithPort(), "", "", "");
    serversModel->append(server);
}

public:
void run()
{
    setupRefreshingThread();
    initGames();
    init();
    refresh();
}

QObject *model() {
    return proxy;
}//serverList->viewModel(); }
};

#endif // MAIN_H
